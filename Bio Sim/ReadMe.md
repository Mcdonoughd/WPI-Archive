#BCB3010

This is a repository for WPI BCB3010 Simulation in Biology for D-term 2018


Here you will find the following Folders:


Bootcamp 1:
&nbsp;	  Several Small Programs made in the first week of the course as a tutorial


Bootcamp 2:
&nbsp;	  Several Slightly Larger Programs made in the second week of the course as a tutorial


Code Review (1,2,& 3):
&nbsp;    A look at another classmates simulation to assist in their coding skills 


Individual Sim:
&nbsp;    A Final Simulation Complete with Hypothesis testing and analysis


Lectures:
&nbsp;    Set of lectures show during class


Other Sims:
&nbsp;    Other Classmates' Simulations


Practice Simulation:
&nbsp;    A simulation colaborated on with a group
