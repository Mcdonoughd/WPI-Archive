# Worcester Polytechnic Institute Class Archive
## By Daniel McDonough

<center>

|Class Name					|HWs	| Slides| QuizKeys  | Books |Discipline |
|---------------------------|:-----:|:-----:|:---------:|:-----:|:----------|
|Intro to Computer Science	|   Y	|   N	|   N		|   N	|Comp. Sci.	|
|Object Oriented Programming|  	Y	|   N	|   Y		|   N	|Comp. Sci.	|
|Systems Programing  		|	Y	|   Y	|   N		|   Y	|Comp. Sci.	|
|Algorithms  				|	Y	|   Y	|   Y		|   Y	|Comp. Sci.	|
|Assembly  					|	Y	|   Y	|   N		|   Y	|Comp. Sci.	|
|Operating Systems  		|	Y	|   Y	|   N		|   Y	|Comp. Sci.	|
|Human Computer Interaction |	Y	|   Y	|   N		|   Y	|Comp. Sci.	|
|Software Engineering  		|	Y	|   Y	|   N		|   Y	|Comp. Sci. |
|Social Implications  		|	Y	|   N	|   N		|   N	|Comp. Sci.	|
|Databases			  		|	Y	|   Y	|   N		|   Y	|Comp. Sci.	|
|Data Mining  				|	Y	|   N	|   N		|   Y	|Comp. Sci.	|
|Calculus (1-4)  			|	N	|   N	|   N		|   Y	|Math		|
|Dicrete Math 				|	N	|   N	|   N		|   Y	|Math		|
|Probabilty  				|	Y	|   Y	|   N		|   Y	|Math		|
|Bio-Technology  			|	Y	|   Y	|   N		|   Y	|Biology	|
|Molecular Biology			|	Y	|   Y	|   N		|   Y	|Biology	|
|Genetics	 				|	Y	|   Y	|   N		|   Y	|Biology	|
|Cell Biology				|	Y	|   Y	|   N		|   Y	|Biology	|
|Biological Simulations		|	Y	|   Y	|   N		|   Y	|Comp. Bio	|
|Chemistry 1				|	N	|   Y	|   N		|   Y	|Chemistry	|
|Cognative Psychology		|	N	|   Y	|   N		|   Y	|Psychology	|

</center>
